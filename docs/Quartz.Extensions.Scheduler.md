<a name='assembly'></a>
# Quartz.Extensions.Scheduler

## Contents

- [QuartzConnectionOptions](#T-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions')
  - [BatchMaxCount](#P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-BatchMaxCount 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions.BatchMaxCount')
  - [BatchTimeWindow](#P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-BatchTimeWindow 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions.BatchTimeWindow')
  - [ConnectionString](#P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-ConnectionString 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions.ConnectionString')
  - [MisfireThreshold](#P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-MisfireThreshold 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions.MisfireThreshold')
  - [RetryCounts](#P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-RetryCounts 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions.RetryCounts')
  - [WaitInterval](#P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-WaitInterval 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions.WaitInterval')
  - [WithinLock](#P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-WithinLock 'Quartz.Extensions.Scheduler.Options.QuartzConnectionOptions.WithinLock')

<a name='T-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions'></a>
## QuartzConnectionOptions `type`

##### Namespace

Quartz.Extensions.Scheduler.Options

<a name='P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-BatchMaxCount'></a>
### BatchMaxCount `property`

##### Summary

Максимальное кол-во задач, которые могут быть захвачены для (запуска) за один раз планировщиком.
Значение по умолчанию 1.

##### Remarks



<a name='P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-BatchTimeWindow'></a>
### BatchTimeWindow `property`

##### Summary

Кол-во миллисекунд от запланированного времени запуска в пределах которого задача может быть получена и запущена.
Значение по умолчанию 0.

##### Remarks



<a name='P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-ConnectionString'></a>
### ConnectionString `property`

##### Summary

Строка подключения к базе данных.

<a name='P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-MisfireThreshold'></a>
### MisfireThreshold `property`

##### Summary

Кол-во миллисекунд, которые планировщик будет \`ожидать\` задачу, до следующего заплонированного запуска, прежде чем задача будет считатся \`misfired\`.
Значение по умолчанию 60000 (60 секунд).

##### Remarks



<a name='P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-RetryCounts'></a>
### RetryCounts `property`

##### Summary

Кол-во повторений задачи, которая завершилась с ошибкой.

<a name='P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-WaitInterval'></a>
### WaitInterval `property`

##### Summary

Интервал времени, который планировщик будет ожидать, перед тем как запустить задачу снова.
Значение по умолчанию 5 секунд.

<a name='P-Quartz-Extensions-Scheduler-Options-QuartzConnectionOptions-WithinLock'></a>
### WithinLock `property`

##### Summary


