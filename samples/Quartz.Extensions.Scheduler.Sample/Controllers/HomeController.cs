﻿using Microsoft.AspNetCore.Mvc;
using Quartz.Extensions.Scheduler.Sample.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Sample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly IQuartzScheduler _scheduler;

        public HomeController(IQuartzScheduler scheduler) => _scheduler = scheduler;

        [HttpGet]
        public string Get() => "Hello from Scheduler.Sample";

        [HttpGet("quartz")]
        public async Task<string> Quartz([FromQuery] string message, CancellationToken cancellationToken = default)
        {
            await _scheduler.Schedule<SimpleService>(
                service => service.PrintMessage(message), DateTimeOffset.Now.AddSeconds(5), cancellationToken
            );

            return "Job has been scheduled";
        }
    }
}
