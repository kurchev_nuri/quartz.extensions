﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Sample.Services
{
    public sealed class SimpleService : IDisposable
    {
        private readonly ILogger<SimpleService> _logger;

        public SimpleService(ILogger<SimpleService> logger) => _logger = logger;

        public void Display(string message) => Console.WriteLine(message);

        public Task PrintMessage(string message)
        {
            _logger.LogError($"`{nameof(SimpleService)}.{nameof(PrintMessage)}` : {message}");

            return Task.CompletedTask;
        }

        public void Dispose() => _logger.LogWarning($"`{nameof(SimpleService)}` has been disposed!");
    }
}
