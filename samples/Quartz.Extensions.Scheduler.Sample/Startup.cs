using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz.Extensions.Scheduler.Extensions;
using Quartz.Extensions.Scheduler.Sample.Services;
using System;
using System.Threading;

namespace Quartz.Extensions.Scheduler.Sample
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddQuartzScheduler(builder =>
            {
                builder.UseMssql(options =>
                {
                    options.RetryCounts = 2;
                    options.BatchTimeWindow = 1_000;
                    options.MisfireThreshold = 120_000;
                    options.BatchMaxCount = ThreadPool.ThreadCount;
                    options.WaitInterval = TimeSpan.FromSeconds(10);
                    options.ConnectionString = Configuration.GetConnectionString("MsSqlQuartzConnectionString");
                });
            });

            services.AddScoped<SimpleService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
