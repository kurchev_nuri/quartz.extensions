﻿using System.Collections.Specialized;

namespace Quartz.Extensions.Scheduler.Builders.Configurations
{
    public interface IQuartzConfigurationBuilder
    {
        NameValueCollection Build();
    }
}
