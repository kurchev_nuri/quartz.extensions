﻿using Microsoft.Extensions.Options;
using Quartz.Extensions.Scheduler.Options;
using System.Collections.Specialized;

namespace Quartz.Extensions.Scheduler.Builders.Configurations.Npgsql
{
    public sealed class NpgsqlQuartzOptionsBuilder : QuartzConfigurationBuilder
    {
        public NpgsqlQuartzOptionsBuilder(IOptions<QuartzConnectionOptions> options)
            : base(options)
        {
        }

        protected override NameValueCollection Properties(QuartzConnectionOptions options)
        {
            return new NameValueCollection
            {
                ["quartz.serializer.type"] = "json",
                ["quartz.jobStore.tablePrefix"] = "QRTZ_",
                ["quartz.jobStore.useProperties"] = "false",
                ["quartz.jobStore.type"] = "Quartz.Impl.AdoJobStore.JobStoreTX, Quartz",
                ["quartz.jobStore.driverDelegateType"] = "Quartz.Impl.AdoJobStore.PostgreSQLDelegate, Quartz",

                ["quartz.jobStore.dataSource"] = "default",
                ["quartz.jobStore.misfireThreshold"] = options.MisfireThreshold.ToString(),
                ["quartz.jobStore.acquireTriggersWithinLock"] = options.WithinLock.ToString(),
                ["quartz.dataSource.default.provider"] = "Npgsql",
                ["quartz.dataSource.default.connectionString"] = options.ConnectionString,
                ["quartz.scheduler.batchTriggerAcquisitionMaxCount"] = options.BatchMaxCount.ToString(),
                ["quartz.scheduler.batchTriggerAcquisitionFireAheadTimeWindow"] = options.BatchTimeWindow.ToString()
            };
        }
    }
}
