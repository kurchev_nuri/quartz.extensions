﻿using Microsoft.Extensions.Options;
using Quartz.Extensions.Scheduler.Options;
using System.Collections.Specialized;

namespace Quartz.Extensions.Scheduler.Builders.Configurations
{
    public abstract class QuartzConfigurationBuilder : IQuartzConfigurationBuilder
    {
        private readonly QuartzConnectionOptions _options;

        protected QuartzConfigurationBuilder(IOptions<QuartzConnectionOptions> options) => _options = options.Value;

        public NameValueCollection Build() => Properties(_options);

        protected abstract NameValueCollection Properties(QuartzConnectionOptions options);
    }
}
