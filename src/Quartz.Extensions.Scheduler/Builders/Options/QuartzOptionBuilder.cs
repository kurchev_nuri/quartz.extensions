﻿using Microsoft.Extensions.DependencyInjection;

namespace Quartz.Extensions.Scheduler.Builders.Options
{
    public class QuartzOptionBuilder
    {
        public QuartzOptionBuilder(IServiceCollection services) => Services = services;

        internal IServiceCollection Services { get; }
    }
}
