﻿namespace Quartz.Extensions.Scheduler.Common
{
    internal static class CommonConsts
    {
        public const string MethodName = nameof(MethodName);
        public const string RetryCounts = nameof(RetryCounts);

        public const string FailTriggerGroup = nameof(FailTriggerGroup);
    }

    internal static class NpgqlConsts
    {
        public const string ResourceDirectory = "Npgsql";
        public const string ScriptsDirectory = "Scripts";
        public const string InitializersDirectory = "Initializers";

        public const string TableCreatingSql = "npgsql_tables_creating.sql";
        public const string TableClearingSql = "npgsql_tables_clearing.sql";
    }

    internal static class MssqlConsts
    {
        public const string ResourceDirectory = "Mssql";
        public const string ScriptsDirectory = "Scripts";
        public const string InitializersDirectory = "Initializers";

        public const string TableCreatingSql = "mssql_tables_creating.sql";
        public const string TableClearingSql = "mssql_tables_clearing.sql";
    }
}
