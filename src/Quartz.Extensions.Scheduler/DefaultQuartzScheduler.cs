﻿using Quartz.Extensions.Scheduler.Common;
using Quartz.Extensions.Scheduler.Exceptions;
using Quartz.Extensions.Scheduler.Jobs;
using Quartz.Extensions.Scheduler.Serializers;
using Quartz.Extensions.Scheduler.Utilities;
using Remote.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler
{
    internal sealed class DefaultQuartzScheduler : IQuartzScheduler
    {
        private readonly IScheduler _scheduler;
        private readonly ISerializer _serializer;

        public DefaultQuartzScheduler(IScheduler scheduler, ISerializer serializer)
        {
            _scheduler = scheduler;
            _serializer = serializer;
        }

        public Task<bool> RemoveJob(string jobId, CancellationToken cancellationToken = default)
        {
            return _scheduler.UnscheduleJob(new TriggerKey(jobId), cancellationToken);
        }

        public async Task<List<string>> GetCurrentlyExecutingJobs(CancellationToken cancellationToken = default)
        {
            var contexts = await _scheduler.GetCurrentlyExecutingJobs(cancellationToken);

            return contexts.Select(context => context.JobDetail.Key.Name).ToList();
        }

        public async Task<string> Enqueue<T>(Expression<Func<T, Task>> methodCall, CancellationToken cancellationToken = default)
        {
            ThrowIfArgumentsAreWrong(methodCall);

            var trigger = CreateTrigger();
            var jobDetail = CreateJob(typeof(FuncTaskJob<T>), _serializer.Serialize(methodCall.ToRemoteLinqExpression()));

            await _scheduler.ScheduleJob(jobDetail, trigger, cancellationToken);

            return trigger.Key.Name;
        }

        public async Task<string> Enqueue<T>(Expression<Action<T>> methodCall, CancellationToken cancellationToken = default)
        {
            ThrowIfArgumentsAreWrong(methodCall);

            var trigger = CreateTrigger();
            var jobDetail = CreateJob(typeof(ActionJob<T>), _serializer.Serialize(methodCall.ToRemoteLinqExpression()));

            await _scheduler.ScheduleJob(jobDetail, trigger, cancellationToken);

            return trigger.Key.Name;
        }

        public async Task<string> Schedule<T>(Expression<Action<T>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken = default)
        {
            ThrowIfArgumentsAreWrong(methodCall);

            var trigger = CreateTrigger(startAt);
            var jobDetail = CreateJob(typeof(ActionJob<T>), _serializer.Serialize(methodCall.ToRemoteLinqExpression()));

            await _scheduler.ScheduleJob(jobDetail, trigger, cancellationToken);

            return trigger.Key.Name;
        }

        public async Task<string> Schedule<T>(Expression<Func<T, Task>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken = default)
        {
            ThrowIfArgumentsAreWrong(methodCall);

            var trigger = CreateTrigger(startAt);
            var jobDetail = CreateJob(typeof(FuncTaskJob<T>), _serializer.Serialize(methodCall.ToRemoteLinqExpression()));

            await _scheduler.ScheduleJob(jobDetail, trigger, cancellationToken);

            return trigger.Key.Name;
        }

        public async Task<string> SchedulePeriodic<T>(Expression<Func<T, Task>> methodCall, TimeSpan interval, CancellationToken cancellationToken = default)
        {
            ThrowIfArgumentsAreWrong(methodCall);

            var key = Sha256Helper.Compute($"{typeof(T).FullName}{ExpressionInfoHelper.GetMemberName(methodCall)}");
            var trigger = CreateTrigger(DateTimeOffset.UtcNow, interval, $"{key}Trigger");
            var jobDetail = CreateJob(typeof(FuncTaskJob<T>), _serializer.Serialize(methodCall.ToRemoteLinqExpression()), $"{key}Job");

            await _scheduler.ScheduleJob(jobDetail, new[] { trigger }, true, cancellationToken);

            return trigger.Key.Name;
        }

        public async Task<string> ScheduleCron<T>(Expression<Func<T, Task>> methodCall, string cronExpression, CancellationToken cancellationToken = default)
        {
            ThrowIfArgumentsAreWrong(methodCall);

            var key = Sha256Helper.Compute($"{typeof(T).FullName}{ExpressionInfoHelper.GetMemberName(methodCall)}");
            var trigger = CreateTrigger(DateTimeOffset.UtcNow, cronExpression, $"{key}Trigger");
            var jobDetail = CreateJob(typeof(FuncTaskJob<T>), _serializer.Serialize(methodCall.ToRemoteLinqExpression()), $"{key}Job");

            await _scheduler.ScheduleJob(jobDetail, new[] { trigger }, true, cancellationToken);

            return trigger.Key.Name;
        }

        #region Private Methods

        private void ThrowIfArgumentsAreWrong<TArgument>(TArgument argument)
        {
            if (_scheduler.IsShutdown)
            {
                throw new SchedulerShutdownException("Планировщик был остановлен.");
            }

            if (EqualityComparer<TArgument>.Default.Equals(argument, default))
            {
                throw new ArgumentNullException(typeof(TArgument).Name, "Выражение `Expression` не может быть пустым.");
            }
        }

        private static ITrigger CreateTrigger()
        {
            return TriggerBuilder.Create()
                .WithIdentity(Guid.NewGuid().ToString("N"))
                .StartNow()
                .Build();
        }

        private static ITrigger CreateTrigger(DateTimeOffset startAt)
        {
            return TriggerBuilder.Create()
                .WithIdentity(Guid.NewGuid().ToString("N"))
                .StartAt(startAt)
                .Build();
        }

        private static ITrigger CreateTrigger(DateTimeOffset startAt, TimeSpan interval, string name)
        {
            return TriggerBuilder.Create()
                .WithIdentity(name)
                .StartAt(startAt)
                .WithSimpleSchedule(builder => builder.WithInterval(interval).RepeatForever())
                .Build();
        }

        private static ITrigger CreateTrigger(DateTimeOffset startAt, string cronExpression, string name)
        {
            return TriggerBuilder.Create()
                .WithIdentity(name)
                .StartAt(startAt)
                .WithCronSchedule(cronExpression, builder => builder.InTimeZone(TimeZoneInfo.Utc))
                .Build();
        }

        private static IJobDetail CreateJob(Type jobType, string value)
        {
            return JobBuilder.Create(jobType)
                .WithIdentity(Guid.NewGuid().ToString("N"))
                .UsingJobData(CommonConsts.RetryCounts, 0)
                .UsingJobData(CommonConsts.MethodName, value)
                .RequestRecovery()
                .Build();
        }

        private static IJobDetail CreateJob(Type jobType, string value, string name)
        {
            return JobBuilder.Create(jobType)
                .WithIdentity(name)
                .UsingJobData(CommonConsts.RetryCounts, 0)
                .UsingJobData(CommonConsts.MethodName, value)
                .RequestRecovery()
                .Build();
        }

        #endregion
    }
}
