﻿using System;
using System.Runtime.Serialization;

namespace Quartz.Extensions.Scheduler.Exceptions
{
    public class BaseQuartzException : Exception
    {
        public BaseQuartzException()
        {
        }

        public BaseQuartzException(string message) : base(message)
        {
        }

        public BaseQuartzException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BaseQuartzException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
