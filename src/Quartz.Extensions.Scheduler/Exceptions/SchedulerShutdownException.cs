﻿using System;
using System.Runtime.Serialization;

namespace Quartz.Extensions.Scheduler.Exceptions
{
    public sealed class SchedulerShutdownException : BaseQuartzException
    {
        public SchedulerShutdownException()
        {
        }

        public SchedulerShutdownException(string message) : base(message)
        {
        }

        public SchedulerShutdownException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public SchedulerShutdownException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
