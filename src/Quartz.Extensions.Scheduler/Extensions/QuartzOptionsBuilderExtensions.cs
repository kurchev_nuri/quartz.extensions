﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Quartz.Extensions.Scheduler.Builders.Configurations;
using Quartz.Extensions.Scheduler.Builders.Configurations.Mssql;
using Quartz.Extensions.Scheduler.Builders.Configurations.Npgsql;
using Quartz.Extensions.Scheduler.Builders.Options;
using Quartz.Extensions.Scheduler.Initializers;
using Quartz.Extensions.Scheduler.Initializers.Mssql;
using Quartz.Extensions.Scheduler.Initializers.Npgsql;
using Quartz.Extensions.Scheduler.Options;
using System;
using System.Collections.Generic;

namespace Quartz.Extensions.Scheduler.Extensions
{
    public static class QuartzOptionsBuilderExtensions
    {
        public static void UseNpgsql(this QuartzOptionBuilder builder, Action<QuartzConnectionOptions> configure)
        {
            ThrowIfOptionsAreNull(builder, configure);

            builder.Services.AddOptions<QuartzConnectionOptions>()
                .Configure(configure)
                .Validate<IValidator<QuartzConnectionOptions>>(Validate);

            builder.Services.AddTransient<IStoreInitializer, QuartzNpgsqlStoreInitializer>();
            builder.Services.AddTransient<IQuartzConfigurationBuilder, NpgsqlQuartzOptionsBuilder>();
        }

        public static void UseNpgsql<TDependency>(this QuartzOptionBuilder builder, Action<QuartzConnectionOptions, TDependency> configure)
            where TDependency : class
        {
            ThrowIfOptionsAreNull(builder, configure);

            builder.Services.AddOptions<QuartzConnectionOptions>()
                .Configure(configure)
                .Validate<IValidator<QuartzConnectionOptions>>(Validate);

            builder.Services.AddTransient<IStoreInitializer, QuartzNpgsqlStoreInitializer>();
            builder.Services.AddTransient<IQuartzConfigurationBuilder, NpgsqlQuartzOptionsBuilder>();
        }

        public static void UseNpgsql<TDep1, TDep2>(this QuartzOptionBuilder builder, Action<QuartzConnectionOptions, TDep1, TDep2> configure)
            where TDep1 : class
            where TDep2 : class
        {
            ThrowIfOptionsAreNull(builder, configure);

            builder.Services.AddOptions<QuartzConnectionOptions>()
                .Configure(configure)
                .Validate<IValidator<QuartzConnectionOptions>>(Validate);

            builder.Services.AddTransient<IStoreInitializer, QuartzNpgsqlStoreInitializer>();
            builder.Services.AddTransient<IQuartzConfigurationBuilder, NpgsqlQuartzOptionsBuilder>();
        }

        public static void UseMssql(this QuartzOptionBuilder builder, Action<QuartzConnectionOptions> configure)
        {
            ThrowIfOptionsAreNull(builder, configure);

            builder.Services.AddOptions<QuartzConnectionOptions>()
                .Configure(configure)
                .Validate<IValidator<QuartzConnectionOptions>>(Validate);

            builder.Services.AddTransient<IStoreInitializer, QuartzMssqlStoreInitializer>();
            builder.Services.AddTransient<IQuartzConfigurationBuilder, MssqlQuartzOptionsBuilder>();
        }

        public static void UseMssql<TDependency>(this QuartzOptionBuilder builder, Action<QuartzConnectionOptions, TDependency> configure)
            where TDependency : class
        {
            ThrowIfOptionsAreNull(builder, configure);

            builder.Services.AddOptions<QuartzConnectionOptions>()
                .Configure(configure)
                .Validate<IValidator<QuartzConnectionOptions>>(Validate);

            builder.Services.AddTransient<IStoreInitializer, QuartzMssqlStoreInitializer>();
            builder.Services.AddTransient<IQuartzConfigurationBuilder, MssqlQuartzOptionsBuilder>();
        }

        public static void UseMssql<TDep1, TDep2>(this QuartzOptionBuilder builder, Action<QuartzConnectionOptions, TDep1, TDep2> configure)
            where TDep1 : class
            where TDep2 : class
        {
            ThrowIfOptionsAreNull(builder, configure);

            builder.Services.AddOptions<QuartzConnectionOptions>()
                .Configure(configure)
                .Validate<IValidator<QuartzConnectionOptions>>(Validate);

            builder.Services.AddTransient<IStoreInitializer, QuartzMssqlStoreInitializer>();
            builder.Services.AddTransient<IQuartzConfigurationBuilder, MssqlQuartzOptionsBuilder>();
        }

        #region Private Methods

        private static void ThrowIfOptionsAreNull<TBuilder, TConfigure>(TBuilder builder, TConfigure configure)
        {
            if (EqualityComparer<TBuilder>.Default.Equals(builder, default))
            {
                throw new ArgumentNullException(nameof(builder), $"Входное значение `{nameof(builder)}` не может быть null или пустым");
            }

            if (EqualityComparer<TConfigure>.Default.Equals(configure, default))
            {
                throw new ArgumentNullException(nameof(configure), $"Входное значение `{nameof(configure)}` не может быть null или пустым");
            }
        }

        private static bool Validate<TOptions>(TOptions options, IValidator<TOptions> validator)
        {
            validator.ValidateAndThrow(options);
            return true;
        }

        #endregion
    }
}
