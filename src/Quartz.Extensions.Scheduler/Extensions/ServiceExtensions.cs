﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Quartz.Extensions.Scheduler.Builders.Configurations;
using Quartz.Extensions.Scheduler.Builders.Options;
using Quartz.Extensions.Scheduler.Factories;
using Quartz.Extensions.Scheduler.HostedServices;
using Quartz.Extensions.Scheduler.Initializers;
using Quartz.Extensions.Scheduler.Initializers.InMemory;
using Quartz.Extensions.Scheduler.Jobs;
using Quartz.Extensions.Scheduler.Recovery;
using Quartz.Extensions.Scheduler.Serializers;
using Quartz.Spi;
using Scrutor;
using System;
using System.Linq;

namespace Quartz.Extensions.Scheduler.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddQuartzScheduler(this IServiceCollection services)
        {
            services.AddSingleton<ISchedulerFactory, DefaultSchedulerFactory>();
            services.AddTransient<IStoreInitializer, InMemoryStoreInitializer>();

            return CommonServices(services);
        }

        public static IServiceCollection AddQuartzScheduler(this IServiceCollection services, Action<QuartzOptionBuilder> configure)
        {
            configure(new QuartzOptionBuilder(services));

            services.AddSingleton<ISchedulerFactory>(provider =>
            {
                var factory = provider.GetRequiredService<IJobFactory>();
                var properties = provider.GetRequiredService<IQuartzConfigurationBuilder>().Build();

                return new DefaultSchedulerFactory(properties, factory, provider);
            });

            return CommonServices(services);
        }

        private static IServiceCollection CommonServices(IServiceCollection services)
        {
            services.AddHostedService<QuartzHostedService>();

            services.AddSingleton<IJobFactory, DefaultJobFactory>();
            services.AddSingleton<IJobRecovery, DefaultJobRecovery>();
            services.AddSingleton<ISerializer, DefaultJsonSerializer>();

            services.AddSingleton(provider => provider.GetRequiredService<ISchedulerFactory>()
                .GetScheduler()
                .GetAwaiter()
                .GetResult()
            );

            services.AddSingleton(typeof(ActionJob<>));
            services.AddSingleton(typeof(FuncTaskJob<>));

            services.AddSingleton<IQuartzScheduler, DefaultQuartzScheduler>();

            return services.Scan(u => u.FromAssemblies(typeof(ServiceExtensions).Assembly)
                .AddClasses(v => v.AssignableTo(typeof(IValidator<>)))
                    .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                    .As(type => type.GetInterfaces().Where(type => type.IsGenericType && type.Name.Contains(nameof(IValidator))))
                    .WithSingletonLifetime()
                );
        }
    }
}
