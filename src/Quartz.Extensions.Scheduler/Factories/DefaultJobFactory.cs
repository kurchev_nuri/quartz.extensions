﻿using Microsoft.Extensions.DependencyInjection;
using Quartz.Spi;
using System;

namespace Quartz.Extensions.Scheduler.Factories
{
    internal sealed class DefaultJobFactory : IJobFactory
    {
        private readonly IServiceProvider _provider;

        public DefaultJobFactory(IServiceProvider provider) => _provider = provider;

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _provider.GetRequiredService(bundle.JobDetail.JobType) as IJob;
        }

        public void ReturnJob(IJob job) => (job as IDisposable)?.Dispose();
    }
}
