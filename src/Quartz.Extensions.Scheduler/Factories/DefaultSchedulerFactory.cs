﻿using Microsoft.Extensions.DependencyInjection;
using Quartz.Core;
using Quartz.Impl;
using Quartz.Spi;
using Quartz.Util;
using System;
using System.Collections.Specialized;

namespace Quartz.Extensions.Scheduler.Factories
{
    internal sealed class DefaultSchedulerFactory : StdSchedulerFactory
    {
        private readonly IJobFactory _factory;
        private readonly IServiceProvider _provider;

        public DefaultSchedulerFactory(IJobFactory factory, IServiceProvider provider)
        {
            _factory = factory;
            _provider = provider;
        }

        public DefaultSchedulerFactory(NameValueCollection props, IJobFactory factory, IServiceProvider provider)
            : base(props)
        {
            _factory = factory;
            _provider = provider;
        }

        protected override IScheduler Instantiate(QuartzSchedulerResources rsrcs, QuartzScheduler qs)
        {
            var scheduler = base.Instantiate(rsrcs, qs);

            scheduler.JobFactory = _factory;

            return scheduler;
        }

        protected override T InstantiateType<T>(Type implementationType)
        {
            var service = _provider.GetService<T>();
            if (service is null)
            {
                service = ObjectUtils.InstantiateType<T>(implementationType);
            }

            return service;
        }
    }
}
