﻿using Microsoft.Extensions.Hosting;
using Quartz.Extensions.Scheduler.Initializers;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.HostedServices
{
    internal sealed class QuartzHostedService : IHostedService
    {
        private readonly IScheduler _scheduler;
        private readonly IStoreInitializer _initializer;

        public QuartzHostedService(IScheduler scheduler, IStoreInitializer initializer)
        {
            _scheduler = scheduler;
            _initializer = initializer;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _initializer.CreateStoreAsync(cancellationToken);
            await _scheduler.Start(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return _scheduler.Shutdown(waitForJobsToComplete: true, cancellationToken);
        }
    }
}
