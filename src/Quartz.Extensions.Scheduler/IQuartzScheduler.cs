﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler
{
    public interface IQuartzScheduler
    {
        Task<bool> RemoveJob(string jobId, CancellationToken cancellationToken = default);

        Task<List<string>> GetCurrentlyExecutingJobs(CancellationToken cancellationToken = default);

        Task<string> Enqueue<T>(Expression<Action<T>> methodCall, CancellationToken cancellationToken = default);

        Task<string> Enqueue<T>(Expression<Func<T, Task>> methodCall, CancellationToken cancellationToken = default);

        Task<string> Schedule<T>(Expression<Action<T>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken = default);

        Task<string> Schedule<T>(Expression<Func<T, Task>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken = default);

        Task<string> SchedulePeriodic<T>(Expression<Func<T, Task>> methodCall, TimeSpan interval, CancellationToken cancellationToken = default);

        Task<string> ScheduleCron<T>(Expression<Func<T, Task>> methodCall, string cronExpression, CancellationToken cancellationToken = default);
    }
}
