﻿using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Initializers
{
    internal interface IStoreInitializer
    {
        Task CleanStoreAsync(CancellationToken cancellationToken = default);

        Task CreateStoreAsync(CancellationToken cancellationToken = default);
    }
}
