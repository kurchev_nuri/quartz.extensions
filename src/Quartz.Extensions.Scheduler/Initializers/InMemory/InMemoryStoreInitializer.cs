﻿using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Initializers.InMemory
{
    internal sealed class InMemoryStoreInitializer : IStoreInitializer
    {
        public Task CleanStoreAsync(CancellationToken cancellationToken = default) => Task.CompletedTask;

        public Task CreateStoreAsync(CancellationToken cancellationToken = default) => Task.CompletedTask;
    }
}
