﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using Quartz.Extensions.Scheduler.Common;
using Quartz.Extensions.Scheduler.Options;
using Quartz.Extensions.Scheduler.Utilities;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Initializers.Mssql
{
    public sealed class QuartzMssqlStoreInitializer : IStoreInitializer
    {
        private readonly IOptionsMonitor<QuartzConnectionOptions> _options;

        public QuartzMssqlStoreInitializer(IOptionsMonitor<QuartzConnectionOptions> options) => _options = options;

        public async Task CreateStoreAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            await using var connection = new SqlConnection(_options.CurrentValue.ConnectionString);

            var subPath = Path.Combine(MssqlConsts.InitializersDirectory, MssqlConsts.ResourceDirectory, MssqlConsts.ScriptsDirectory);

            var script = await EmbeddedResourcesReader.ReadResourceAsStringAsync(
                Path.Combine(subPath, MssqlConsts.TableCreatingSql), typeof(QuartzMssqlStoreInitializer).Assembly
            );

            await connection.ExecuteAsync(script);
        }

        public async Task CleanStoreAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            await using var connection = new SqlConnection(_options.CurrentValue.ConnectionString);

            var script = await GetCleanStoreSqlAsync(cancellationToken);

            await connection.ExecuteAsync(script);
        }

        #region Static Methods

        public static string GetCleanStoreSql()
        {
            var subPath = Path.Combine(MssqlConsts.InitializersDirectory, MssqlConsts.ResourceDirectory, MssqlConsts.ScriptsDirectory);

            return EmbeddedResourcesReader.ReadResourceAsString(
                Path.Combine(subPath, MssqlConsts.TableClearingSql), typeof(QuartzMssqlStoreInitializer).Assembly
            );
        }

        public static async Task<string> GetCleanStoreSqlAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var subPath = Path.Combine(MssqlConsts.InitializersDirectory, MssqlConsts.ResourceDirectory, MssqlConsts.ScriptsDirectory);

            return await EmbeddedResourcesReader.ReadResourceAsStringAsync(
                Path.Combine(subPath, MssqlConsts.TableClearingSql), typeof(QuartzMssqlStoreInitializer).Assembly
            );
        }

        #endregion
    }
}
