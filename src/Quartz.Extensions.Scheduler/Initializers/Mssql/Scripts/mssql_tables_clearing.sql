﻿ IF OBJECT_ID('[dbo].[QRTZ_TRIGGERS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_TRIGGERS]
	END;

IF OBJECT_ID('[dbo].[QRTZ_CRON_TRIGGERS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_CRON_TRIGGERS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_SIMPLE_TRIGGERS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_SIMPLE_TRIGGERS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_SIMPROP_TRIGGERS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_SIMPROP_TRIGGERS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_LOCKS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_LOCKS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_JOB_DETAILS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_JOB_DETAILS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_CALENDARS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_CALENDARS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_BLOB_TRIGGERS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_BLOB_TRIGGERS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_FIRED_TRIGGERS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_FIRED_TRIGGERS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_PAUSED_TRIGGER_GRPS]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_PAUSED_TRIGGER_GRPS];
	END;

IF OBJECT_ID('[dbo].[QRTZ_SCHEDULER_STATE]') IS NOT NULL
	BEGIN
		DELETE FROM [dbo].[QRTZ_SCHEDULER_STATE];
	END;