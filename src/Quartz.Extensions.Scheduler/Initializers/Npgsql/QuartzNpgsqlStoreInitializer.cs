﻿using Dapper;
using Microsoft.Extensions.Options;
using Npgsql;
using Quartz.Extensions.Scheduler.Common;
using Quartz.Extensions.Scheduler.Options;
using Quartz.Extensions.Scheduler.Utilities;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Initializers.Npgsql
{
    public sealed class QuartzNpgsqlStoreInitializer : IStoreInitializer
    {
        private readonly IOptionsMonitor<QuartzConnectionOptions> _options;

        public QuartzNpgsqlStoreInitializer(IOptionsMonitor<QuartzConnectionOptions> options) => _options = options;

        public async Task CreateStoreAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            await using var connection = new NpgsqlConnection(_options.CurrentValue.ConnectionString);

            var subPath = Path.Combine(NpgqlConsts.InitializersDirectory, NpgqlConsts.ResourceDirectory, NpgqlConsts.ScriptsDirectory);

            var script = await EmbeddedResourcesReader.ReadResourceAsStringAsync(
                Path.Combine(subPath, NpgqlConsts.TableCreatingSql), typeof(QuartzNpgsqlStoreInitializer).Assembly
            );

            await connection.ExecuteAsync(script);
        }

        public async Task CleanStoreAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            await using var connection = new NpgsqlConnection(_options.CurrentValue.ConnectionString);

            var script = await GetCleanStoreSqlAsync(cancellationToken);

            await connection.ExecuteAsync(script);
        }

        #region Static Methods

        public static string GetCleanStoreSql()
        {
            var subPath = Path.Combine(MssqlConsts.InitializersDirectory, MssqlConsts.ResourceDirectory, MssqlConsts.ScriptsDirectory);

            return EmbeddedResourcesReader.ReadResourceAsString(
                Path.Combine(subPath, NpgqlConsts.TableClearingSql), typeof(QuartzNpgsqlStoreInitializer).Assembly
            );
        }

        public static async Task<string> GetCleanStoreSqlAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var subPath = Path.Combine(NpgqlConsts.InitializersDirectory, NpgqlConsts.ResourceDirectory, NpgqlConsts.ScriptsDirectory);

            return await EmbeddedResourcesReader.ReadResourceAsStringAsync(
                Path.Combine(subPath, NpgqlConsts.TableClearingSql), typeof(QuartzNpgsqlStoreInitializer).Assembly
            );
        }

        #endregion
    }
}
