﻿DO $$
BEGIN
	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_triggers')
		THEN DELETE FROM qrtz_triggers;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_cron_triggers')
		THEN DELETE FROM qrtz_cron_triggers;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_simple_triggers')
		THEN DELETE FROM qrtz_simple_triggers;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='QRTZ_SIMPROP_TRIGGERS')
		THEN DELETE FROM QRTZ_SIMPROP_TRIGGERS;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_locks')
		THEN DELETE FROM qrtz_locks;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_job_details')
		THEN DELETE FROM qrtz_job_details;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_calendars')
		THEN DELETE FROM qrtz_calendars;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_blob_triggers')
		THEN DELETE FROM qrtz_blob_triggers;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_fired_triggers')
		THEN DELETE FROM qrtz_fired_triggers;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_paused_trigger_grps')
		THEN DELETE FROM qrtz_paused_trigger_grps;
	END IF;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='qrtz_scheduler_state')
		THEN DELETE FROM qrtz_scheduler_state;
	END IF;
END;
$$