﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz.Extensions.Scheduler.Common;
using Quartz.Extensions.Scheduler.Recovery;
using Quartz.Extensions.Scheduler.Serializers;
using Quartz.Extensions.Scheduler.Utilities;
using Remote.Linq;
using Remote.Linq.Expressions;
using System;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Jobs
{
    [DisallowConcurrentExecution]
    [PersistJobDataAfterExecution]
    internal sealed class FuncTaskJob<TService> : IJob
    {
        private readonly IJobRecovery _recovery;
        private readonly ISerializer _serializer;
        private readonly IServiceProvider _provider;
        private readonly ILogger<FuncTaskJob<TService>> _logger;

        public FuncTaskJob(ISerializer serializer, IJobRecovery recovery, IServiceProvider provider, ILogger<FuncTaskJob<TService>> logger)
        {
            _logger = logger;
            _provider = provider;
            _recovery = recovery;
            _serializer = serializer;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var value = context.JobDetail
                .JobDataMap
                .GetString(CommonConsts.MethodName);

            var result = _serializer.Deserialize<LambdaExpression>(value);
            var method = result.ToLinqExpression() as System.Linq.Expressions.Expression<Func<TService, Task>>;

            using var scope = _provider.CreateScope();

            try
            {
                await method?.Compile().Invoke(scope.ServiceProvider.GetRequiredService<TService>());
            }
            catch (Exception exception)
            {
                var methodDescription = $"{typeof(TService).Name}.{ExpressionInfoHelper.GetMemberName(method)}";

                _logger.LogError(exception, $"Фоновая задача `{methodDescription}` завершилась с ошибкой.");

                await _recovery.RescheduleJobAsync(context, methodDescription);
            }
        }
    }
}
