﻿using System;

namespace Quartz.Extensions.Scheduler.Options
{
    public class QuartzConnectionOptions
    {
        /// <summary>
        /// Кол-во повторений задачи, которая завершилась с ошибкой.
        /// </summary>
        public int? RetryCounts { get; set; }

        /// <summary>
        /// Строка подключения к базе данных.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Максимальное кол-во задач, которые могут быть захвачены для (запуска) за один раз планировщиком.
        /// Значение по умолчанию 1.
        /// </summary>
        /// <remarks><see href="https://www.quartz-scheduler.net/documentation/quartz-3.x/configuration/reference.html#quartz-scheduler-batchtriggeracquisitionmaxcount">Original definition.</see></remarks>
        public int BatchMaxCount { get; set; } = 1;

        /// <summary>
        /// Кол-во миллисекунд от запланированного времени запуска в пределах которого задача может быть получена и запущена.
        /// Значение по умолчанию 0.
        /// </summary>
        /// <remarks><see href="https://www.quartz-scheduler.net/documentation/quartz-3.x/configuration/reference.html#quartz-scheduler-batchtriggeracquisitionfireaheadtimewindow">Original definition</see></remarks>
        public int BatchTimeWindow { get; set; } = 0;

        /// <summary>
        /// Кол-во миллисекунд, которые планировщик будет `ожидать` задачу, до следующего заплонированного запуска, прежде чем задача будет считатся `misfired`.
        /// Значение по умолчанию 60000 (60 секунд).
        /// </summary>
        /// <remarks><see href="https://www.quartz-scheduler.net/documentation/quartz-3.x/configuration/reference.html#ramjobstore">Original definition.</see></remarks>
        public int MisfireThreshold { get; set; } = 60000;

        /// <summary>
        /// Интервал времени, который планировщик будет ожидать, перед тем как запустить задачу снова.
        /// Значение по умолчанию 5 секунд.
        /// </summary>
        public TimeSpan WaitInterval { get; set; } = TimeSpan.FromSeconds(5);

        /// <summary>
        /// <see href="https://www.quartz-scheduler.net/documentation/quartz-3.x/configuration/reference.html#quartz-jobstore-acquiretriggerswithinlock">Original definition.</see>
        /// </summary>
        internal bool WithinLock => BatchMaxCount > 1;
    }
}
