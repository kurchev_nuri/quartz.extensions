﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Quartz.Extensions.Scheduler.Common;
using Quartz.Extensions.Scheduler.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Recovery
{
    internal sealed class DefaultJobRecovery : IJobRecovery
    {
        private readonly ILogger<DefaultJobRecovery> _logger;
        private readonly IOptions<QuartzConnectionOptions> _options;

        public DefaultJobRecovery(ILogger<DefaultJobRecovery> logger, IOptions<QuartzConnectionOptions> options)
        {
            _logger = logger;
            _options = options;
        }

        public Task RescheduleJobAsync(IJobExecutionContext context, string methodDescription, CancellationToken cancellationToken = default)
        {
            if (!_options.Value.RetryCounts.HasValue)
            {
                return Task.CompletedTask;
            }

            var count = context.JobDetail.JobDataMap.GetIntValue(CommonConsts.RetryCounts);
            if (count > _options.Value.RetryCounts.Value)
            {
                _logger.LogError($"Исчерпано кол-во попыток перезапуска для задачи `{methodDescription}`. Кол-во попыток: `{_options.Value.RetryCounts}`");
                return Task.CompletedTask;
            }

            context.JobDetail.JobDataMap.Put(CommonConsts.RetryCounts, ++count);

            var trigger = TriggerBuilder.Create()
                .WithIdentity(Guid.NewGuid().ToString("N"), CommonConsts.FailTriggerGroup)
                .StartAt(DateTime.Now.Add(_options.Value.WaitInterval))
                .Build();

            _logger.LogInformation($"Попытка запланировать задачу `{methodDescription}` через заданный интервал `{_options.Value.WaitInterval}`.");

            return context.Scheduler.RescheduleJob(context.Trigger.Key, trigger, cancellationToken);
        }
    }
}
