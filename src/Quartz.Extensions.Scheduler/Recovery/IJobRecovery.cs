﻿using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Recovery
{
    internal interface IJobRecovery
    {
        Task RescheduleJobAsync(IJobExecutionContext context, string methodDescription, CancellationToken cancellationToken = default);
    }
}
