﻿using Newtonsoft.Json;
using Quartz.Extensions.Scheduler.Serializers.Helpers;

namespace Quartz.Extensions.Scheduler.Serializers
{
    internal sealed class DefaultJsonSerializer : ISerializer
    {
        readonly JsonSerializerSettings _settings = JsonSerializerHelper.DefaultSerializerSettings;

        public string Serialize<TInput>(TInput value) => JsonConvert.SerializeObject(value, _settings);

        public TResult Deserialize<TResult>(string value) => JsonConvert.DeserializeObject<TResult>(value, _settings);
    }
}
