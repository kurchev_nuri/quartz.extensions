﻿using Newtonsoft.Json;
using Remote.Linq;
using System;
using System.Threading;

namespace Quartz.Extensions.Scheduler.Serializers.Helpers
{
    internal static class JsonSerializerHelper
    {
        static readonly Lazy<JsonSerializerSettings> _lazy = new(InitDefaultSettings, LazyThreadSafetyMode.PublicationOnly);

        public static JsonSerializerSettings DefaultSerializerSettings => _lazy.Value;

        static JsonSerializerSettings InitDefaultSettings() => new JsonSerializerSettings().ConfigureRemoteLinq();
    }
}
