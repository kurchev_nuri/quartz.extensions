﻿namespace Quartz.Extensions.Scheduler.Serializers
{
    internal interface ISerializer
    {
        public string Serialize<TInput>(TInput value);

        TResult Deserialize<TResult>(string value);
    }
}
