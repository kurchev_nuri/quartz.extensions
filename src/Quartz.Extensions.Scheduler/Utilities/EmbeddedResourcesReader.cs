﻿using Microsoft.Extensions.FileProviders;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Utilities
{
    internal static class EmbeddedResourcesReader
    {
        public static async Task<string> ReadResourceAsStringAsync(string subPath, Assembly assembly)
        {
            await using var stream = GetStream(subPath, assembly);
            using var reader = new StreamReader(stream);

            return await reader.ReadToEndAsync();
        }

        public static string ReadResourceAsString(string subPath, Assembly assembly)
        {
            using var stream = GetStream(subPath, assembly);
            using var reader = new StreamReader(stream);

            return reader.ReadToEnd();
        }

        private static Stream GetStream(string subPath, Assembly assembly)
        {
            if (string.IsNullOrEmpty(subPath))
            {
                throw new ArgumentNullException(nameof(subPath), $"Аргумент {nameof(subPath)} не должен быть пустым.");
            }

            if (assembly == default)
            {
                throw new ArgumentNullException(nameof(assembly), $"Аргумент {nameof(assembly)} не должен быть пустым.");
            }

            return new ManifestEmbeddedFileProvider(assembly).GetFileInfo(subPath).CreateReadStream();
        }
    }
}
