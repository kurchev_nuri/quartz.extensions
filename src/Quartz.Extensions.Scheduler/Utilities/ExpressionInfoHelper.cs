﻿using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Utilities
{
    internal static class ExpressionInfoHelper
    {
        public static string GetMemberName(Expression<Action> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression), "Переданный аргумент не может быть равен null.");
            }

            return GetMemberName(expression.Body);
        }

        public static string GetMemberName(Expression<Func<Task>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression), "Переданный аргумент не может быть равен null.");
            }

            return GetMemberName(expression.Body);
        }

        public static string GetMemberName<T>(Expression<Func<T, Task>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression), "Переданный аргумент не может быть равен null.");
            }

            return GetMemberName(expression.Body);
        }

        public static string GetMemberName<T>(Expression<Func<T, CancellationToken, Task>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression), "Переданный аргумент не может быть равен null.");
            }

            return GetMemberName(expression.Body);
        }

        public static string GetMemberName<T, TResult>(Expression<Func<T, CancellationToken, Task<TResult>>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression), "Переданный аргумент не может быть равен null.");
            }

            return GetMemberName(expression.Body);
        }

        public static string GetMemberName<T>(Expression<Action<T>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression), "Переданный аргумент не может быть равен null.");
            }

            return GetMemberName(expression.Body);
        }

        private static string GetMemberName(Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression), "Переданный аргумент не может быть равен null.");
            }

            // Reference type property or field
            if (expression is MemberExpression memberExpression)
            {
                return memberExpression.Member.Name;
            }

            // Reference type method
            if (expression is MethodCallExpression methodCallExpression)
            {
                return methodCallExpression.Method.Name;
            }

            // Property, field of method returning value type
            if (expression is UnaryExpression unaryExpression)
            {
                return GetMemberName(unaryExpression);
            }

            return string.Empty;
        }

        private static string GetMemberName(UnaryExpression unaryExpression)
        {
            if (unaryExpression.Operand is MethodCallExpression methodExpression)
            {
                return methodExpression.Method.Name;
            }

            return ((MemberExpression)unaryExpression.Operand).Member.Name;
        }
    }
}
