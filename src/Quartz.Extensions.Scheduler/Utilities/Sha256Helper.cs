﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Quartz.Extensions.Scheduler.Utilities
{
    internal static class Sha256Helper
    {
        public static string Compute(string rawData)
        {
            using var sha256Hash = SHA256.Create();
            var bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

            var result = bytes.Aggregate(new StringBuilder(),
                (builder, value) => builder.Append(value.ToString("x2"))
            );

            return result.ToString();
        }
    }
}
