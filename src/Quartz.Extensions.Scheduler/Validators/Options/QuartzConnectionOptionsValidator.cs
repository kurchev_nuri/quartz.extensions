﻿using FluentValidation;
using Quartz.Extensions.Scheduler.Options;

namespace Quartz.Extensions.Scheduler.Validators.Options
{
    internal sealed class QuartzConnectionOptionsValidator : AbstractValidator<QuartzConnectionOptions>
    {
        public QuartzConnectionOptionsValidator()
        {
            RuleFor(options => options.ConnectionString)
                .NotEmpty()
                .WithMessage($"Входное значение `{nameof(QuartzConnectionOptions.ConnectionString)}` не может быть null или пустым");

            RuleFor(options => options.RetryCounts.Value)
                .GreaterThan(0)
                .WithMessage($"Входное значение `{nameof(QuartzConnectionOptions.RetryCounts)}` должно быть больше чем `0`")
                .When(options => options.RetryCounts.HasValue);
        }
    }
}
