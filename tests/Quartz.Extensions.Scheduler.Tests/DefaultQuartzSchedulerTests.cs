﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using Quartz.Extensions.Scheduler.Exceptions;
using Quartz.Extensions.Scheduler.Tests.Helpers;
using Quartz.Extensions.Scheduler.Tests.Resources;
using Quartz.Extensions.Scheduler.Tests.Utilities;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Quartz.Extensions.Scheduler.Tests
{
    public sealed class DefaultQuartzSchedulerTests : BaseDefaultQuartzSchedulerTest
    {
        public DefaultQuartzSchedulerTests()
        {
            SetupSerializer<SimpleService>(Serializer, new Mock<ILogger<SimpleService>>().Object);
        }

        [Theory(DisplayName = "Проверка некорректных входных параметров метода Enqueue. Планирование метода из сервиса.")]
        [IncorrectDataForEnqueueServiceMethod]
        internal async Task EnqueueServiceMethod_WithIncorrectParams_ThrowArgumentNullException(Expression<Action<SimpleService>> methodCall, CancellationToken cancellationToken)
        {
            Func<Task<string>> method = () => Scheduler.Enqueue(methodCall, cancellationToken);

            await method.Should().ThrowAsync<ArgumentNullException>();
        }

        [Theory(DisplayName = "Проверка метода Enqueue при выключенном планировщике. Планирование метода из сервиса.")]
        [CorrectDataForEnqueueServiceMethod]
        internal async Task EnqueueServiceMethod_WithShutdownedScheduler_ThrowSchedulerShutdownException(Expression<Action<SimpleService>> methodCall, CancellationToken cancellationToken)
        {
            QuartzScheduler.Setup(u => u.IsShutdown).Returns(true);

            Func<Task<string>> method = () => Scheduler.Enqueue(methodCall, cancellationToken);

            await method.Should().ThrowAsync<SchedulerShutdownException>();
        }

        [Theory(DisplayName = "Проверка некорректных входных параметров метода Enqueue. Планирование асинхронного метода из сервиса.")]
        [IncorrectDataForEnqueueServiceMethod]
        internal async Task EnqueueServiceAsyncMethod_WithIncorrectParams_ThrowArgumentNullException(Expression<Func<SimpleService, Task>> methodCall, CancellationToken cancellationToken)
        {
            Func<Task<string>> method = () => Scheduler.Enqueue(methodCall, cancellationToken);

            await method.Should().ThrowAsync<ArgumentNullException>();
        }

        [Theory(DisplayName = "Проверка метода Enqueue при выключенном планировщике. Планирование асинхронного метода из сервиса.")]
        [CorrectDataForEnqueueServiceAsyncMethod]
        internal async Task EnqueueServiceAsyncMethod_WithShutdownedScheduler_ThrowSchedulerShutdownException(Expression<Func<SimpleService, Task>> methodCall, CancellationToken cancellationToken)
        {
            QuartzScheduler.Setup(u => u.IsShutdown).Returns(true);

            Func<Task<string>> method = () => Scheduler.Enqueue(methodCall, cancellationToken);

            await method.Should().ThrowAsync<SchedulerShutdownException>();
        }

        [Theory(DisplayName = "Проверка некорректных входных параметров метода Schedule. Планирование метода из сервиса.")]
        [IncorrectDataForScheduleServiceMethod]
        internal async Task ScheduleServiceMethod_WithIncorrectParams_ThrowArgumentNullException(Expression<Action<SimpleService>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken)
        {
            Func<Task<string>> method = () => Scheduler.Schedule(methodCall, startAt, cancellationToken);

            await method.Should().ThrowAsync<ArgumentNullException>();
        }

        [Theory(DisplayName = "Проверка метода Schedule при выключенном планировщике. Планирование метода из сервиса.")]
        [CorrectDataForScheduleServiceMethod]
        internal async Task ScheduleServiceMethod_WithShutdownedScheduler_ThrowSchedulerShutdownException(Expression<Action<SimpleService>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken)
        {
            QuartzScheduler.Setup(u => u.IsShutdown).Returns(true);

            Func<Task<string>> method = () => Scheduler.Schedule(methodCall, startAt, cancellationToken);

            await method.Should().ThrowAsync<SchedulerShutdownException>();
        }

        [Theory(DisplayName = "Проверка некорректных входных параметров метода Schedule. Планирование асинхронного метода из сервиса.")]
        [IncorrectDataForScheduleServiceMethod]
        internal async Task ScheduleServiceAsyncMethod_WithIncorrectParams_ThrowArgumentNullException(Expression<Func<SimpleService, Task>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken)
        {
            Func<Task<string>> method = () => Scheduler.Schedule(methodCall, startAt, cancellationToken);

            await method.Should().ThrowAsync<ArgumentNullException>();
        }

        [Theory(DisplayName = "Проверка метода Schedule при выключенном планировщике. Планирование асинхронного метода из сервиса.")]
        [CorrectDataForScheduleServiceAsyncMethod]
        internal async Task ScheduleServiceAsyncMethod_WithShutdownedScheduler_ThrowSchedulerShutdownException(Expression<Func<SimpleService, Task>> methodCall, DateTimeOffset startAt, CancellationToken cancellationToken)
        {
            QuartzScheduler.Setup(u => u.IsShutdown).Returns(true);

            Func<Task<string>> method = () => Scheduler.Schedule(methodCall, startAt, cancellationToken);

            await method.Should().ThrowAsync<SchedulerShutdownException>();
        }
    }
}
