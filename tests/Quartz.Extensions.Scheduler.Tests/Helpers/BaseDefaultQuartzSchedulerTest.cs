﻿using Bogus;
using Moq;
using Quartz.Extensions.Scheduler.Serializers;
using System;

namespace Quartz.Extensions.Scheduler.Tests.Helpers
{
    public abstract class BaseDefaultQuartzSchedulerTest
    {
        public BaseDefaultQuartzSchedulerTest()
        {
            Serializer = new Mock<ISerializer>();
            QuartzScheduler = new Mock<IScheduler>();

            Scheduler = new DefaultQuartzScheduler(QuartzScheduler.Object, Serializer.Object);
        }

        internal Mock<ISerializer> Serializer { get; set; }

        internal Mock<IScheduler> QuartzScheduler { get; set; }

        internal DefaultQuartzScheduler Scheduler { get; set; }

        internal static void SetupSerializer<TService>(Mock<ISerializer> serializer, params object[] arguments)
            where TService : class
        {
            var randomizer = new Randomizer();

            serializer.Setup(u => u.Serialize(It.IsAny<TService>())).Returns(randomizer.String2(10));
            serializer.Setup(u => u.Deserialize<TService>(It.IsAny<string>()))
                .Returns(
                    Activator.CreateInstance(typeof(TService), arguments) as TService
                );
        }
    }
}
