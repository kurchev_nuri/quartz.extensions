﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Quartz.Extensions.Scheduler.Tests.Resources
{
    internal sealed class CorrectDataForEnqueueAsyncMethodAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            using var cancellationTokenSource = new CancellationTokenSource();

            Func<Task> action = () => Task.CompletedTask;
            Expression<Func<Task>> expression = () => action();

            yield return new object[] { expression, cancellationTokenSource.Token };
        }
    }
}
