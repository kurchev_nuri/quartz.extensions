﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using Xunit.Sdk;

namespace Quartz.Extensions.Scheduler.Tests.Resources
{
    internal sealed class CorrectDataForEnqueueMethodAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            using var cancellationTokenSource = new CancellationTokenSource();
            
            Action action = () => { };
            Expression<Action> expression = () => action();

            yield return new object[] { expression, cancellationTokenSource.Token };
        }
    }
}
