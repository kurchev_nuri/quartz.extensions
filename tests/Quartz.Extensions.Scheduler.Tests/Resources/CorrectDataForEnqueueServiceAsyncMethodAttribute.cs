﻿using Quartz.Extensions.Scheduler.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Quartz.Extensions.Scheduler.Tests.Resources
{
    internal sealed class CorrectDataForEnqueueServiceAsyncMethodAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            using var cancellationTokenSource = new CancellationTokenSource();
            Expression<Func<SimpleService, Task>> expression = service => service.DoEnqueueAsync();

            yield return new object[] { expression, cancellationTokenSource.Token };
        }
    }
}
