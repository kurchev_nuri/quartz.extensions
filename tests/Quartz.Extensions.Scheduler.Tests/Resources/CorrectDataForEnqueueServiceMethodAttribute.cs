﻿using Quartz.Extensions.Scheduler.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using Xunit.Sdk;

namespace Quartz.Extensions.Scheduler.Tests.Resources
{
    internal sealed class CorrectDataForEnqueueServiceMethodAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            using var cancellationTokenSource = new CancellationTokenSource();
            Expression<Action<SimpleService>> expression = service => service.DoEnqueue();

            yield return new object[] { expression, cancellationTokenSource.Token };
        }
    }
}
