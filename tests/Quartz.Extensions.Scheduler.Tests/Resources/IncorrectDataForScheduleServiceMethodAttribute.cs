﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Xunit.Sdk;

namespace Quartz.Extensions.Scheduler.Tests.Resources
{
    internal sealed class IncorrectDataForScheduleServiceMethodAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            using var cancellationTokenSource = new CancellationTokenSource();

            yield return new object[] { default, DateTimeOffset.Now.AddSeconds(5), cancellationTokenSource.Token };
        }
    }
}
