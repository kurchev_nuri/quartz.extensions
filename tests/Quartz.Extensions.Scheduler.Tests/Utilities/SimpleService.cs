﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Quartz.Extensions.Scheduler.Tests.Utilities
{
    public class SimpleService
    {
        private readonly ILogger<SimpleService> _logger;

        public SimpleService(ILogger<SimpleService> logger) => _logger = logger;

        public virtual void DoEnqueue()
        {
            _logger.Log(LogLevel.Information, default, "DoEnqueue has been Invoked", default, (message, exception) => message);
        }

        public virtual Task DoEnqueueAsync()
        {
            _logger.Log(LogLevel.Information, default, "DoEnqueue has been Invoked", default, (message, exception) => message);

            return Task.CompletedTask;
        }

        public virtual void DoSchedule()
        {
            _logger.Log(LogLevel.Information, default, "DoSchedule has been Invoked", default, (message, exception) => message);
        }

        public virtual Task DoScheduleAsync()
        {
            _logger.Log(LogLevel.Information, default, "DoScheduleAsync has been Invoked", default, (message, exception) => message);

            return Task.CompletedTask;
        }
    }
}
